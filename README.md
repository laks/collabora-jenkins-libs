# Collabora-Jenkins-libs

Collection of shared libraries for use in Jenkinsfile(s).

## Documentation

https://jenkins.io/doc/book/pipeline/shared-libraries/

### How to use collabora-jenkins-lib shared-library:

If your repo is private or internal, first ensure `Jenkins` added as a member to your gitlab repo (`Settings->Members`) and webhooks (`Settings->Integrations`) are enabled.
Now edit the content of your Jenkinsfile with the following

	@Library('collabora-jenkins-libs') _

Then simple invoke library function checkoutCollaboraGitlab(). 

The library call checkoutCollaboraGitlab() takes single argument and two optional arguments. They are:

- git repo name. (qa/shared-lib-example)
- optionally, branch name. (default: master)
- optionally, dest directory to clone the repo.

For example, the below call, 

```	checkoutCollaboraGitlab('qa/shared-lib-example') ```

will fetch _gitlab.collabora.com/qa/shared-lib-example.git_ repo.

#### Environment variables:

Four main environmental are used with-in this library.

```
	env.gitlabSourceRepoSshUrl
	env.gitlabSourceBranch
	env.gitlabTargetRepoSshUrl
	env.gitlabTargetBranch
```

All of them are optional.

#### PreBuildMerge:

gitlab PreBuildMerge class allow you to perform a merge to a particular branch before building.
	
###### Case1:

If both `env.gitlabSourceBranch` and `env.gitlabTargetBranch` are set, then `checkoutCollaboraGitlab()`
	will first perform a merge of `env.gitlabSourceBranch` with `env.gitlabTargetBranch`. Only if the
        merge passes, it will proceed with the build. Otherwise, git merge conflict errors will be displayed.

For example:
```
		  environment {
		        gitlabSourceBranch = "devel"
			gitlabTargetBranch = "master"
		  }
```
Adding above environment block will ensure, with every change of `devel branch` , Jenkins will perform a merge
	with the `master branch`, and try to perform a build if the merge is successful. In this case both,
	`devel` and `master` are part of **same** repo.

###### Case2:
Its not nesscary both branch should be on same repo. PrebuildMerge can be applied across **different** repo. 
	This is accomplished with the help of `env.gitlabSourceRepoSshUrl` and `env.gitlabTargetRepoSshUrl`

For example:
```
	environment {
        	gitlabSourceRepoSshUrl = 'git@gitlab.collabora.com:user1/repo1'
			gitlabTargetRepoSshUrl = 'git@gitlab.collabora.com:user2/repo1'
	        gitlabSourceBranch = "master"
			gitlabTargetBranch = "master"
	  	}
```

Adding above environment block will ensure, with every change of `user1/repo1 master` branch , Jenkins will perform a merge
	with the `user2/repo1 master` branch, and try to perform a build if the merge is successful. In this case both,
	'master' are part of **different** git repo.

###### Case3:
PreBuildMerge is skipped, if none of the four environmental variable `env.gitlabSourceRepoSshUrl,env.gitlabSourceBranch, env.gitlabTargetRepoSshUrl,env.gitlabTargetBranch` is used.


### Examples:

#### Example 1:

Gitlab: [shared-lib-example](https://gitlab.collabora.com/qa/shared-lib-example/blob/master/Jenkinsfile)
	Jenkins: [Job](https://jenkins.collabora.co.uk/job/Collabora/job/collabora-jenkins-libs/job/shared-lib-example/)

This does simple build whenever git push or merge request are created.

#### Example-2: 

Gitlab: [shared-lib-example-2](https://gitlab.collabora.com/qa/shared-lib-example-2/blob/master/merge_req/Jenkinsfile)
Jenkins: [Job](https://jenkins.collabora.co.uk/job/Collabora/job/collabora-jenkins-libs/job/shared-lib-example-mr/)

This does build whenever git push or merge request are created but It also uses
`PrebuildMerge` option. Every commit from sourcebranch `devel` applied to targetbranch `master` and if there is no merge issue then build will proceed. Note that both source-branch and target-branch belongs to same git repo.

#### Example-3: 
Gitlab : [shared-lib-example]( https://gitlab.collabora.com/shared-lib/shared-lib-example/blob/master/diff_repo/Jenkinsfile)
Jenkins:[Job](https://jenkins.collabora.co.uk/job/Collabora/job/collabora-jenkins-libs/job/grp-shared-lib-example/)

This is similar to above example except we have two **different** git repo instead of one. This does build whenever git push or merge request are created but It also uses
`PrebuildMerge` option. Every commit from `sourcebranch` applied to `targetbranch` and if there is no merge issue then build will proceed. 
